FROM alpine:edge
RUN apk -v --no-cache add \
        git \
        less \
        openssh-client \
        py-pip \
        python \
        && \
    pip install --upgrade awsebcli
VOLUME /root/.aws
